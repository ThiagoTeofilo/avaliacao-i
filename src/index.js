import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import addClassSaved from './components/Functions/LS'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

window.onload = function() {
  let product_card = document.querySelectorAll("main .product-card")
  for (var i = 0; i < product_card.length; i++) {
    var id = product_card[i].id
    if(localStorage.getItem(id) != null) {
      addClassSaved(id, "main")
    }
  }
}

// --- Scrolls --- //

window.addEventListener('scroll', function() {
  const body = document.querySelector("body")
  if (window.scrollY >= 5) {
      body.classList.add('scroll')
  } else {
      body.classList.remove('scroll')
  }
})

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
