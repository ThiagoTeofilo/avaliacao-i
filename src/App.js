import './App.css'
import './assets/fonts/icomoon/style.css'
import Products from './components/Products/products'
import Wishlist from './components/Wishlist/wishlist'
import { useState } from 'react'
import SimpleSlider from './components/SimpleSlider'

function App() {
  var [open, setOpen] = useState(false)
  const products_id = []
  const products_names = []
  const products_playlists = []

  // Product Regist (import img in App.css file)
  // Punk Rock
  const products_id_Punk_Rock = [
    'RoadToRuin',
    'LeaveHome',
    'RoquetToRussia',
    'LondonCalling',
    'StayFree',
    'TheSkysGoneOut'
  ]
  const products_names_Punk_Rock = [
    'Ramones - Road to Ruin',
    'Ramones - Leave Home',
    'Ramones - Rocket to Rusia',
    'The Clash - London Calling',
    'The Clash - Stay Here',
    'Bauhaus - The Skys Gone Out'
  ]
  const products_playlists_Punk_Rock = [
    "I Just Want To Have Something To Do / I Wanted Everything / Don't Come Close",
    'Glad to See You Go / Gimme Gimme Shock Treatment / I Remenber You',
    'Cretin Hop / Rockaway Beach / Here Today, Gone Tomorrow',
    'London Calling / Brand New Cadillac / Jimmy Jazz',
    "Safe European Home / I'm So Bored With de USA / Complete Control",
    'Third Uncle / Silent Hedges / In the Night'
  ]

  // Heavy Metal
  const products_id_Heavy_Metal = [
    'Trash',
    'Powerslave',
    'BraveNewWorld',
    'AceOfSpades',
    'BlackInBlack',
    'Paranoid'
  ]
  const products_names_Heavy_Metal = [
    'Alice Cooper - Trash',
    'Iron Maiden - Powerslave',
    'Iron Maiden - Brave New World',
    'Motörhead - Ace Of Spades',
    'AC/DC - Black in Black',
    'Black Sabbath - Paranoid'
  ]
  const products_playlists_Heavy_Metal = [
    'Poison / Spark in the Dark / House of Fire',
    'Aces Hight / 2 Minutes to Midnight / Losfer Words',
    'The Wicker Man / Ghost of the Navigator / Brave New World',
    'Ace Of Spades / Love Me Like a Reptile / Shoot You in the Back',
    'Shoot to Thrill / What Do You Do For Money Honey / Givin The Dog A Bone',
    'War Pigs / Paranoid / Planet Caravan'
  ]

  // Adicionar ao array de músicas geral
  Array.prototype.push.apply(products_id, products_id_Punk_Rock)
  Array.prototype.push.apply(products_id, products_id_Heavy_Metal)

  Array.prototype.push.apply(products_names, products_names_Punk_Rock)
  Array.prototype.push.apply(products_names, products_names_Heavy_Metal)

  Array.prototype.push.apply(products_playlists, products_playlists_Punk_Rock)
  Array.prototype.push.apply(products_playlists, products_playlists_Heavy_Metal)
  // /Product Regist

  // Import products to wishlist
  let desired_products_id = []
  let desired_products_names = []
  let desired_products_playlists = []
  for (var i = 0; i < products_id.length; i++) {
    if (localStorage.getItem(products_id[i]) != null) {
      desired_products_id.push(products_id[i])
      desired_products_names.push(products_names[i])
      desired_products_playlists.push(products_playlists[i])
    }
  }

  // Header Texts
  const header_texts = [
    'Sejam bem vindos a nossa loja de discos em ascensão',
    'Sua primeira compra? ganhe um brinde surpresa',
    'Caso queira testar antes da compra, entre em contato'
  ]

  return (
    <div id="App">
      <header>
        <div id="top-bar">
          <h1 id="logotype">
            <span>Pró</span>Disk
          </h1>
          <button
            id="wishlist-btn"
            onClick={function () {
              setOpen(open === true ? false : true)
            }}
          >
            <div class="icon-love"></div>
            <span>Lista de Desejos</span>
          </button>
        </div>
        <div id="header-content">
          <SimpleSlider images="h-img1 h-img2 h-img3" texts={header_texts} />
        </div>
      </header>
      <dialog open={open}>
        <button
          id="wishlist-btn-close"
          onClick={function () {
            setOpen(open === true ? false : true)
          }}
        >
          Voltar
        </button>
        <Wishlist
          id={desired_products_id}
          names={desired_products_names}
          playlists={desired_products_playlists}
        />
      </dialog>
      <main>
        <h1>Ordenado Por Estilo Musical</h1>
        <section class="products-section" id="Punk-Rock-section">
          <h2 class="title-section">
            Punk Rock <span>1974 - 1980</span>
          </h2>
          <div class="grid-section grid">
            <Products
              id={products_id_Punk_Rock}
              names={products_names_Punk_Rock}
              playlists={products_playlists_Punk_Rock}
            />
          </div>
        </section>
        <section class="products-section" id="Heavy-Metal-section">
          <h2 class="title-section">
            Heavy Metal <span>1970 - 1976</span>
          </h2>
          <div class="grid-section grid">
            <Products
              id={products_id_Heavy_Metal}
              names={products_names_Heavy_Metal}
              playlists={products_playlists_Heavy_Metal}
            />
          </div>
        </section>
      </main>
    </div>
  )
}

export default App
