import './products.css'
import addClassSaved from '../Functions/LS'

function Products(props) {
  const product_qnt = props.names.length
  const max_char_band_name = 11
  const max_char_album_name = 11
  const max_char_music_name = 9
  let rows = []
  for (let i = 0; i < product_qnt; i++) {
    // Separar a lista de música de cada album para modelar o card do produto
    let product_playlist = props.playlists[i].split(' / ')

    // Criar as estruturas dos cards de produto para exportação
    rows.push(
      <div id={props.id[i]} class={'product-card grid ' + props.id[i]}>
        <div class="product-img"></div>
        <div class="product-info">
          <h3>
            {verifyName(props.names[i].split(' - ')[0], max_char_band_name)} -{' '}
            <br />
            {verifyName(props.names[i].split(' - ')[1], max_char_album_name)}
          </h3>
          <ul>
            <li>
              <span class="icon-music"></span>
              {verifyName(product_playlist[0], max_char_music_name)}
            </li>
            <li>
              <span class="icon-music"></span>
              {verifyName(product_playlist[1], max_char_music_name)}
            </li>
            <li>
              <span class="icon-music"></span>
              {verifyName(product_playlist[2], max_char_music_name)}
            </li>
            <li>
              <span class="icon-music"></span>More...
            </li>
          </ul>
          <div class="product-buttons">
            <button
              class="add_wish_btn"
              onClick={() =>
                saveToWishlist(props.id[i], props.names[i], product_playlist)
              }
            >
              <div class="icon-love"></div>
              <span>Add aos Favoritos</span>
            </button>
            <button class="buy_btn">
              <div class="icon-shopping-cart"></div>
              <span>Ir até a Compra</span>
            </button>
          </div>
        </div>
      </div>
    )
  }

  return <>{rows}</>
}

function verifyName(name, max_char) {
  let arrayName = name.split('')
  let newArrayName = []
  var reducedName = ''
  if (arrayName.length > max_char) {
    for (var i = 0; i < max_char; i++) {
      newArrayName.push(arrayName[i])
      reducedName = reducedName + newArrayName[i]
    }
    reducedName = reducedName + '...'
  } else {
    reducedName = name
  }
  return reducedName
}

function saveToWishlist(id, name, playlist) {
  let data = {
    id: id,
    name: name,
    playlist: playlist.join(' - ')
  }
  if (localStorage.getItem(id)) {
    localStorage.removeItem(id)

    // Eliminar o produto da wishlist imediatamente se a propia estiver aberta
    if ((id != null) & (document.querySelector('dialog .' + id) != null)) {
      document.querySelector('dialog .' + id).classList.add('invisible')
    }
  } else {
    localStorage.setItem(id, JSON.stringify(data))
  }
  addClassSaved(id, 'main')
}

export default Products
