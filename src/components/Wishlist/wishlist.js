import './wishlist.css'
import Products from '../Products/products'

function Wishlist(props) {
  var elements
  if (props.id[0] !== undefined) {
    elements = (
      <Products id={props.id} names={props.names} playlists={props.playlists} />
    )
  } else {
    elements = <h3 class="wishlist-mensage">Nenhum Produto Foi Salvo!</h3>
  }
  return (
    <section class="products-section" id="wishlist">
      <h1>Lista de Desejos</h1>
      <div class="grid-section grid">{elements}</div>
    </section>
  )
}

export default Wishlist
