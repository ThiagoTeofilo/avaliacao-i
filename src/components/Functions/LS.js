function addClassSaved(id, local) {
  if (localStorage.getItem(id) == null) {
    document.querySelector(local + " ." + id).classList.remove("saved")
    document.querySelector(local + " ." + id + " .add_wish_btn span").innerHTML = "Add aos Favoritos"
  } else {
    document.querySelector(local + " ." + id).classList.add("saved")
    document.querySelector(local + " ." + id + " .add_wish_btn span").innerHTML = "Tirar dos Favoritos"
  } 
}



export default addClassSaved