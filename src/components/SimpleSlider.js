import React from 'react'
import Slider from 'react-slick'
import './SliderImg.css'

export default function SimpleSlider(props) {
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    cssEase: 'linear',
    fade: true
  }
  var rows = []
  var images = props.images
  let img_array = images.split(' ')
  for (var i = 0; i < img_array.length; i++) {
    rows.push(
      <div class="slider-img">
        <div class={img_array[i]}>
          <h1 class="slider-content">{props.texts[i]}</h1>
        </div>
      </div>
    )
  }
  return (
    <div class="Slider">
      <Slider {...settings}>{rows}</Slider>
    </div>
  )
}
